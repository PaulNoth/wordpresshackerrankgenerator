# README #
This is a template generator for Hackerrank solutions at [pidanic.com](pidanic.com)

At the moment it reads Hackerrank challenges from local stylesheet file which contains all relevant information for generations. Sources are read from local cloned [Github repository](https://github.com/PaulNoth/hackerrank).

In the end it should read explanations from Evernote notebook, sources directly from GitHub and post it automatically on my personal page.