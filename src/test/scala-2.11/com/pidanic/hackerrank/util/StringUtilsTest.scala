package com.pidanic.hackerrank.util

import org.scalatest.{Matchers, FlatSpec}

class StringUtilsTest extends FlatSpec with Matchers {

  "convertToGitHubConvention" should "make a string lowercase" in {
    StringUtils.convertToGitHubConvention("SampleNAme") should be ("samplename")
  }

  "convertToGitHubConvention" should "replace all spaces with '_'" in {
    StringUtils.convertToGitHubConvention("Sample NAme") should be ("sample_name")
  }

  "convertToHackerrankConvention" should "make a string lowercase" in {
    StringUtils.convertToHackerrankConvention("SampleNAme") should be ("samplename")
  }

  "convertToHackerrankConvention" should "replace all spaces with '-'" in {
    StringUtils.convertToHackerrankConvention("Sample NAme") should be ("sample-name")
  }
}
