package com.pidanic.hackerrank.util

import java.io.File

import org.scalatest.{Matchers, FlatSpec}

class FileHelperTest extends FlatSpec with Matchers {

  "GetExtension" should "throw NullPointerException when no file specified" in {
    a [NullPointerException] should be thrownBy {
      FileHelper.getExtension(null)
    }
  }

  "GetExtension" should "return empty string if file without extension" in {
    FileHelper.getExtension(new File("sample")) should be ("")
  }

  "GetExtension" should "return correct file extension" in {
    FileHelper.getExtension(new File("sample.txt")) should be ("txt")
  }

  "GetExtension" should "return correct file extension if there are more dots in file name" in {
    FileHelper.getExtension(new File("sample.java.rb.txt")) should be ("txt")
  }
}
