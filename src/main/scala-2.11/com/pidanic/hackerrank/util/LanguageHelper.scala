package com.pidanic.hackerrank.util

object LanguageHelper {

  def getLanguage(extension: String): String = {
    extension match {
      case "scala" => "Scala"
      case "rb" => "Ruby"
      case "java" => "Java"
      case "js" => "JavaScript"
      case "sh" => "Bash"
      case "txt" => "Plain Text"
      case "sql" => "SQL"
    }
  }

}
