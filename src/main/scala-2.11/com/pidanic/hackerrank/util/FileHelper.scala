package com.pidanic.hackerrank.util

import java.io.{FileWriter, File}

import scala.io.Source

object FileHelper {
  def writeToFile(name: String, content: String): Unit = {
    val fw = new FileWriter(name)
    fw.write(content)
    fw.flush()
    fw.close()
  }

  def writeToFile(name: String, content: List[String]): Unit = {
    val fw = new FileWriter(name)
    content.foreach(line => fw.write(line + "\n"))
    fw.flush()
    fw.close()
  }

  def getExtension(file: File): String = {
    if(file == null) {
      throw new NullPointerException("file is null")
    }
    val fileName = file.getName
    if(!(fileName contains '.')) {
      return ""
    }
    file.getName.substring(fileName.lastIndexOf('.') + 1)
  }

  def getLines(name: String): List[String] = {
    Source.fromFile(new File(name)).getLines().toList
  }

  def containsDirectories(dir: File): Boolean = {
    (dir.listFiles())(0).isDirectory
  }
}
