package com.pidanic.hackerrank

import java.io.File

import com.pidanic.hackerrank.util.FileHelper
import com.typesafe.config.ConfigFactory

object Challenges {

  private val config = ConfigFactory.load()

  val allChallenges: List[Challenge] = challengesFromCsv(config.getString("CHALLENGES"))

  val solvedChallenges: List[Challenge] = challengesFromCsv(config.getString("SOLVED"))

  private def challengesFromCsv(fileName: String): List[Challenge] = {
    val withoutHeader = FileHelper.getLines("data" + File.separator + fileName).tail
    withoutHeader.map(parseSolution)
  }

  private def parseSolution(line: String): Challenge = {
    val parts = line.split(";")
    val name = parts(0)
    val domain = parts(5)
    val subdomain = parts(6)
    val points = if(!parts(1).isEmpty) parts(1).toInt else 0
    val difficultyPoints = if(!parts(2).isEmpty) parts(2).toInt else 0
    val difficulty = parts(3)
    val url = parts(4)
    Challenge(name, domain, subdomain, points, difficultyPoints, difficulty, url)
  }

  def getByName(name: String): Option[Challenge] = {
    allChallenges.find(nameCompare(_, name))
  }

  def getSolvedByName(name: String): Option[Challenge] = {
    solvedChallenges.find(nameCompare(_, name))
  }

  private def nameCompare(challenge: Challenge, name: String): Boolean = {
    challenge.name.compareTo(name) == 0
  }
}
