package com.pidanic.hackerrank

import java.io.File

import com.pidanic.hackerrank.evernote.EvernoteWrapper
import com.pidanic.hackerrank.util.{LanguageHelper, FileHelper, StringUtils}

import scala.io.Source

trait TemplateGenerator {
  def createTemplate(dir: File, challengeInfo: Challenge): String
}

object TemplateGenerator {

  def getInstance(evernoteWrapper: EvernoteWrapper): TemplateGenerator = {
    new TemplateGeneratorImpl(evernoteWrapper)
  }

  private class TemplateGeneratorImpl(evernote: EvernoteWrapper) extends TemplateGenerator {
    override def createTemplate(dir: File, challengeInfo: Challenge): String = {
      println("dirName: " + dir.getName + ", files: " + dir.listFiles.length)
      getProblemStatement(challengeInfo.name) + getSolution(challengeInfo.name) + getUl(dir.listFiles().toList) + gitHubRef(challengeInfo) +
        dir.listFiles().map(codeTemplate).mkString("\n")
    }

    private def getProblemStatement(challenge: String): String = {
      val hackerrankName = StringUtils.convertToHackerrankConvention(challenge)
      s"""<strong>Problem Statement</strong>
          |A description of the problem can be found on <a href="https://www.hackerrank.com/challenges/$hackerrankName" target="_blank">Hackerrank</a>.
          |\n""".stripMargin
    }

    private def codeTemplate(file: File): String = {
      val lang = LanguageHelper.getLanguage(FileHelper.getExtension(file))
      val langId = lang.toLowerCase
      val source = Source.fromFile(file).getLines().mkString("\n").replaceAll("\t", " "*4)
      s"""<strong id="$langId">$lang</strong>
          |<pre class="font-size:16 lang:$lang decode:true " title="$lang Solution" >
          |$source
          |</pre>
      """.stripMargin
    }

    private def getUl(files: List[File]): String = {
      val extensions = files.map(FileHelper.getExtension)
      val lis = extensions.map(getLi).mkString("\n")
      s"""I created solution in:<ul>
          |$lis
          |</ul>\n""".stripMargin
    }

    private def getLi(extension: String): String = {
      val lang = LanguageHelper.getLanguage(extension)
      val langId = lang.toLowerCase
      s"""<li><a href="#$langId">$lang</a></li>"""
    }

    private def getSolution(challengeName: String): String = {
      "<strong>Solution</strong>\n" + evernote.getNoteContentByNoteName(challengeName) + "\n"
    }

    private def gitHubRef(challengeInfo: Challenge): String = {
      val domain = StringUtils.convertToGitHubConvention(challengeInfo.domain)
      val subdomain = StringUtils.convertToGitHubConvention(challengeInfo.subdomain)
      val name = StringUtils.convertToGitHubConvention(challengeInfo.name)
      s"""All solutions are also available on my <a href="https://github.com/PaulNoth/hackerrank/tree/master/practice/$domain/$subdomain/$name" target="_blank">GitHub</a>.
    \n\n""".stripMargin
    }
  }
}
