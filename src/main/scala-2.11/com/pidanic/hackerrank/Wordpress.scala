package com.pidanic.hackerrank

import java.io.File

import com.pidanic.hackerrank.util.{LanguageHelper, FileHelper, StringUtils}

import scala.collection.mutable.ListBuffer

class Wordpress(private val challenges: List[Challenge]) {

  private val gitHubMapChallenges = challenges.map(
    info => Tuple2(StringUtils.convertToGitHubConvention(info.name), info)).toMap

  private val maxLength = maxDifficultyLength()

  def createTitles(): List[String] = {
    challenges.map(info => "Hackerrank - " + info.name)
  }

  def createSlugs(): List[String] = {
    challenges.map(info => "/blog/hackerrank-" + StringUtils.slugify(info.name) + "/")
  }

  def createRefs(): List[String] = {
    //val slugs = createSlugs()
    val groupsByDomain = challenges.groupBy(_.domain)
    val keys = groupsByDomain.keySet
    val refs = keys.toList.map(key => {
      val domain = "<h2>" + key + "</h2>"
      val subdomainsGroup = groupsByDomain.get(key).get.groupBy(_.subdomain)
      val subdomains = subdomainsGroup.keySet
      val subdomainsHs = subdomains.toList.map(subdomainKey => {
        val h4 = "<h4>" + subdomainKey + "</h4>"
        val ul = "<ul>"
        val ulClose = "</ul>"
        val sortedChallengesByDifficulty = subdomainsGroup.get(subdomainKey).get
          .sortWith((challenge1, challenge2) => challenge1.difficultyPoints < challenge2.difficultyPoints)
        val slugs = sortedChallengesByDifficulty.map(challenge => {
          val slug = StringUtils.slugify(challenge.name)
          "\t<li><a href=\"/blog/hackerrank-" + slug + "/\">[Difficulty " + prependSpaces(challenge.difficultyPoints) + "" + challenge.difficultyPoints + "] " +
            challenge.name + " (" + challenge.points + " pt" + pluralize(challenge.points) +")</a></li>"
        })
        h4 :: ul :: slugs ::: List(ulClose)
      }).flatten
      domain :: subdomainsHs
    }).flatten
    refs
  }

  def createPageDescriptions(dir: File): List[String] = {
    val result: ListBuffer[String] = ListBuffer.empty
    if(FileHelper.containsDirectories(dir)) {
      dir.listFiles().foreach(file => {
        result ++= createPageDescriptions(file)
      })
    } else {
      val description = createPageDescription(dir)
      result += description
    }
    result.toList
  }

  private def createPageDescription(dir: File): String = {
    val extensions = dir.listFiles().map(FileHelper.getExtension)
    val languages = extensions.map(LanguageHelper.getLanguage)
    val challenge = gitHubMapChallenges.get(dir.getName)
    val description = if(challenge.isDefined) {
      "Solution of Hackerrank " + challenge.get.name + " challenge in " + languages.mkString(", ") + " with explanation."
    } else {
      println(dir.getName + " not found")
      ""
    }
    return description
  }

  private def maxDifficultyLength(): Int = {
    challenges.map(_.difficultyPoints).max.toString.length
  }

  private def pluralize(points: Int): String = {
    if(points != 1) {
      "s"
    } else {
      ""
    }
  }

  private def prependSpaces(points: Int): String = {
    val spaces = maxLength - points.toString.length
    "&nbsp;" * spaces
  }

  def reloadChallenges(challenges: List[Challenge]): Wordpress = {
    new Wordpress(challenges)
  }
}
