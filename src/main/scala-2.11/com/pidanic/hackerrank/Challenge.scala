package com.pidanic.hackerrank

case class Challenge(name: String, domain: String, subdomain: String, points: Int,
                     difficultyPoints: Int, difficulty: String, url: String)
