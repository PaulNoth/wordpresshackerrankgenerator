package com.pidanic.hackerrank

import java.io.File

import com.pidanic.hackerrank.evernote.EvernoteWrapper
import com.pidanic.hackerrank.util.{StringUtils, FileHelper}

object Main {

  def main(args: Array[String]) {
    val solutionsDirPath = args(0)
    val challengesDescPath = args(1)
    val solutionDir = new File(solutionsDirPath)

    val challenges = Challenges.allChallenges
    val gitHubMapChallenges = challenges.map(info => Tuple2(StringUtils.convertToGitHubConvention(info.name), info)).toMap

    //println(gitHubMapChallenges.keys.mkString("|"))

    val wordPress = new Wordpress(challenges)
    FileHelper.writeToFile("titles.txt", wordPress.createTitles())
    FileHelper.writeToFile("slugs.txt", wordPress.createSlugs())
    FileHelper.writeToFile("refs.txt", wordPress.createRefs())
    FileHelper.writeToFile("descriptions.txt", wordPress.createPageDescriptions(solutionDir))

    createTemplates(gitHubMapChallenges, solutionDir)
  }

  private def createTemplates(gitHubMapChallenges: Map[String, Challenge], dir: File): Unit = {
    val templateGenerator = TemplateGenerator.getInstance(EvernoteWrapper.getInstance())
    if (FileHelper.containsDirectories(dir)) {
      dir.listFiles().foreach(createTemplates(gitHubMapChallenges, _))
    } else {
      val challengeInfo = getChallengeInfo(gitHubMapChallenges, dir.getName)
      if (challengeInfo.isDefined) {
        FileHelper.writeToFile(dir.getName + ".html", templateGenerator.createTemplate(dir, challengeInfo.get))
      } else {
        println("Description for '" + dir.getName + "' not found")
      }
    }
  }

  def getChallengeInfo(gitHubMapChallenges: Map[String, Challenge], directoryName: String): Option[Challenge] = {
    gitHubMapChallenges.get(directoryName)
  }
}
