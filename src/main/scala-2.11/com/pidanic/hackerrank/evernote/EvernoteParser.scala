package com.pidanic.hackerrank.evernote

import scala.xml.XML

private[evernote] class EvernoteParser(noteContent: String) {

  private val content = removeHeader()

  private[evernote] def removeHeader(): String = {
    noteContent.replaceAll("<\\?xml.*>", "").replaceAll("<!DOCTYPE.*>", "").trim
  }

  def getTextContent: String = {
    XML.loadString(content).text
  }
}
