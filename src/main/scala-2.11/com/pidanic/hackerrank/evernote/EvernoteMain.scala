package com.pidanic.hackerrank.evernote

import scala.xml.XML

object EvernoteMain extends App {
  val evernote = EvernoteWrapper.getInstance()
  val content = evernote.getNoteContentByNoteName("Test Note")
  println(content)
  //println(new EvernoteParser(content).getTextContent)

  val noteExample =
    s"""<?xml version="1.0" encoding="UTF-8"?>
    |<!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">
    |<en-note><div>Test note text asd <strong>aspibdsa</strong> bsapi </div><div>baspou basp baso basoudb <br clear="none"/></div></en-note>"""
      .stripMargin

  val parser = new EvernoteParser(noteExample)
  println(parser.removeHeader())

  val xmlContent = XML.loadString(
    "<en-note><div>Test note text asd <strong>aspibdsa</strong> bsapi </div><div>baspou basp baso basoudb <br clear=\"none\"/></div></en-note>")
  println(xmlContent)

  xmlContent.child.foreach(node => {
    println(node)
  })

}
