package com.pidanic.hackerrank.evernote

import com.evernote.auth.{EvernoteAuth, EvernoteService}
import com.evernote.clients.ClientFactory
import com.evernote.edam.`type`.Notebook
import com.evernote.edam.notestore.{NotesMetadataResultSpec, NoteFilter}
import com.github.scribejava.apis.EvernoteApi
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.model.Token
import com.typesafe.config.ConfigFactory

trait EvernoteWrapper {
  def getNoteContentByNoteName(noteName: String): String
}

object EvernoteWrapper {
  private val CONSUMER_KEY = "paulnoth"
  private val CONSUMER_SECRET = "df61eee379c7cc4a"
  private val DEVELOPER_TOKEN =
    "S=s1:U=920d5:E=159fddba055:C=152a62a7390:P=1cd:A=en-devtoken:V=2:H=65379de22f77d211ff47dc36e182631a"
  private val DEVELOPER_TOKEN_PRODUCTION =
    "S=s330:U=365e4db:E=15a51e9838d:C=152fa3855b0:P=1cd:A=en-devtoken:V=2:H=7035454af8807315c374e70f3b107c21"

  private val offset = 0
  private val maxNotes = 100

  private val configuration = ConfigFactory.load()

  def getInstance(): EvernoteWrapper = {
    new EvernoteWrapperDeveloperToken
  }

  private class EvernoteWrapperDeveloperToken extends EvernoteWrapper {
    private val configService = configuration.getString("EVERNOTE_SERVICE")
    private val defaultNotebook = configuration.getString("NOTEBOOK")

    private val evernoteAuth =
      if(configService.toLowerCase == "production") {
        new EvernoteAuth(EvernoteService.PRODUCTION, EvernoteWrapper.DEVELOPER_TOKEN_PRODUCTION)
      } else {
        new EvernoteAuth(EvernoteService.SANDBOX, EvernoteWrapper.DEVELOPER_TOKEN)
      }
    private val clientFactory = new ClientFactory(evernoteAuth)
    private val noteStore = clientFactory.createNoteStoreClient()
    private val notebookGuid = getDefaultNotebook.getGuid

    private def getDefaultNotebook: Notebook = {
      val allNotebooks = noteStore.listNotebooks()
      val notebookIndex = (0 until allNotebooks.size).find(allNotebooks.get(_).getName == defaultNotebook)
      if(notebookIndex.isDefined) {
        allNotebooks.get(notebookIndex.get)
      } else {
        throw new IllegalArgumentException(defaultNotebook + " does not exist in " + configService + " store")
      }
    }

    override def getNoteContentByNoteName(noteName: String): String = {
      val noteFilter = getNoteFilter(noteName)

      val notesMetadataResultSpec = getMetadataResultSpec
      val notesMetadataList = noteStore.findNotesMetadata(noteFilter, offset, maxNotes, notesMetadataResultSpec)
      val notesMetadata = notesMetadataList.getNotes

      println("Found " + notesMetadata.size + " notes (" + notesMetadata.toString + ") for note with name: " + noteName)
      val resultContent = new StringBuilder
      (0 until notesMetadata.size).foreach(i => {
        val noteMeta = notesMetadata.get(i)
        val noteGuid = noteMeta.getGuid
        val note = noteStore.getNote(noteGuid, true, false, false, false)
        val parser = new EvernoteParser(note.getContent)
        resultContent ++= parser.getTextContent
      })
      resultContent.toString
    }

    private def getNoteFilter(noteName: String): NoteFilter = {
      val noteFilter = new NoteFilter()
      noteFilter.setWords(noteName)
      noteFilter.setNotebookGuid(notebookGuid)
      noteFilter
    }

    private def getMetadataResultSpec: NotesMetadataResultSpec = {
      val resultSpec = new NotesMetadataResultSpec
      resultSpec.setIncludeNotebookGuid(true)
      resultSpec.setIncludeTitle(true)
      resultSpec.setIncludeTagGuids(false)
      resultSpec.setIncludeContentLength(false)
      resultSpec.setIncludeCreatedIsSet(false)
      resultSpec
    }
  }

  private class EvernoteWrapperOauth extends EvernoteWrapper {
    private val service = EvernoteService.PRODUCTION
    private val oauthService = new ServiceBuilder().provider(classOf[EvernoteApi])
      .apiKey(EvernoteWrapper.CONSUMER_KEY)
      .apiSecret(EvernoteWrapper.CONSUMER_SECRET)
      .build()

    println(oauthService.getVersion)

    val requestToken = oauthService.getRequestToken
    val token = requestToken.getToken
    val secret = requestToken.getSecret
    val scribeRequestToken = new Token(token, secret)

    val scribeAccessToken = oauthService.getAccessToken(scribeRequestToken, null)

    val evernoteAuth = EvernoteAuth.parseOAuthResponse(service, scribeAccessToken.getRawResponse)

    val noteStoreClient = new ClientFactory(evernoteAuth).createNoteStoreClient()

    private val clientFactory = new ClientFactory(evernoteAuth)
    private val noteStore = clientFactory.createNoteStoreClient()

    override def getNoteContentByNoteName(noteName: String): String = {
      ""
    }
  }
}
