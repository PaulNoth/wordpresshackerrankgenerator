name := "WordpressHackerrankTemplateGenerator"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
libraryDependencies += "com.evernote" % "evernote-api" % "1.25.1"
libraryDependencies += "com.github.scribejava" % "scribejava" % "2.1.0"
libraryDependencies += "com.github.scribejava" % "scribejava-core" % "2.1.0"
libraryDependencies += "com.github.scribejava" % "scribejava-apis" % "2.1.0"
libraryDependencies += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.5"
libraryDependencies += "com.typesafe" % "config" % "1.3.0"